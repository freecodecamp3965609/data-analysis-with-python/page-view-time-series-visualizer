# Page View Time Series Visualizer

This is the boilerplate for the Page View Time Series Visualizer project. Instructions for building your project can be found at https://www.freecodecamp.org/learn/data-analysis-with-python/data-analysis-with-python-projects/page-view-time-series-visualizer


## Note from Vincent:

Like the same issue for the previous exercise "Medical Data Visualizer", installing seaborn 0.9.0 will cause fatal compatibility issues with numpy.  Therefore I have to modify the version seaborn==0.13.2 in "requirement.txt".
