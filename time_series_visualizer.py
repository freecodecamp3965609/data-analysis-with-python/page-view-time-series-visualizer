import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

# Import data (Make sure to parse dates. Consider setting index column to 'date'.)
df = pd.read_csv('fcc-forum-pageviews.csv', parse_dates=['date'], index_col='date')

# Clean data
df = df.query("value >= value.quantile(0.025) & value <= value.quantile(0.975)")


def draw_line_plot():
    # Draw line plot

    fig = df.plot(y='value', legend=False, figsize=(15, 5), title="Daily freeCodeCamp Forum Page Views 5/2016-12/2019", xlabel="Date", ylabel="Page Views", colormap="Set1", rot=0).get_figure()




    # Save image and return fig (don't change this part)
    fig.savefig('line_plot.png')
    return fig

def draw_bar_plot():
    # Copy and modify data for monthly bar plot
    df_bar = df.copy()
    df_bar['Years'], df_bar['Months'] = df.index.year, df.index.month
    df_bar = df_bar.groupby(['Years','Months']).mean().reset_index()
    df_bar = df_bar.pivot(index='Years', columns='Months', values='value')
    month_dict = {i: pd.to_datetime(str(i), format='%m').strftime('%B') for i in range(1, 13)}
    df_bar.rename(columns=month_dict, inplace=True)

    # Draw bar plot
    fig = df_bar.plot(kind='bar', stacked=False, ylabel='Average Page Views', figsize=(9,7)).get_figure()




    # Save image and return fig (don't change this part)
    fig.savefig('bar_plot.png')
    return fig

def draw_box_plot():
    # Prepare data for box plots (this part is done!)
    df_box = df.copy()
    df_box.reset_index(inplace=True)
    df_box['year'] = [d.year for d in df_box.date]
    df_box['month'] = [d.strftime('%b') for d in df_box.date]

    # Draw box plots (using Seaborn)
    fig, ax = plt.subplots(1, 2, figsize=(15,5))
    month_order = [pd.to_datetime(str(i), format='%m').strftime('%b') for i in range(1, 13)]
    sns.boxplot(data=df_box, x="year", hue="year", y="value", legend=False, palette='colorblind', ax=ax[0])
    ax[0].set_title('Year-wise Box Plot (Trend)')
    ax[0].set_xlabel('Year')
    sns.boxplot(data=df_box, x="month", hue="month", y="value", order=month_order, legend=False, palette='husl', ax=ax[1])
    ax[1].set_title('Month-wise Box Plot (Seasonality)')
    ax[1].set_xlabel('Month')
    [[ax[i].set_ylim(0, 200000), ax[i].set_ylabel('Page Views')] for i in range(2)]
    [a.set_yticks(range(0, 200001, 20000)) for a in ax]
    plt.rc('font', size=6)
    plt.tight_layout()




    # Save image and return fig (don't change this part)
    fig.savefig('box_plot.png')
    return fig
